.. Sphinx-blek-theme documentation master file, created by
   sphinx-quickstart on Sun Oct  1 21:18:03 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Sphinx-blek-theme's documentation!
=============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:


.. code-block:: python

   # ok
   def hallo(a=1):
       """
       what is this?
       """
       num = 3
       print(f"a={a+num}")
       return "yes"
   
   for i in [1, 2, 3]:
       hallo(i)




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
