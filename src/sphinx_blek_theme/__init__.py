from os import path


def add_author_to_context(app, pagename, templatename, context, doctree):
    context['author'] = app.config.author


def setup(app):
    app.connect("html-page-context", add_author_to_context)
    app.add_html_theme('blek', path.abspath(path.dirname(__file__)))
